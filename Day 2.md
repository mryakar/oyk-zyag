### Anti Detection Teknikleri

- The Rootkit Arsenal: Escape and Evasion: Escape and Evasion in the Dark Corners of the System

    1. Obfuscation
    2. Encoding
        BASE64 encoding tekniği neden kullanılır?
            Kullanım amacı data transferi esnasında her türlü veri tipi desteklenmez. Desteklenen karakterlere indirgemenk için kullanılır.
    3. Code Encryption
        Pack/Crypter
            Packer yazılımı tasarlamayı dene. 

    Malware Detection
        1. Signature-based
        2. Anomaly-based

- Yara: Kötü amaçlı yazılım tanımlama ve sınıflandırmak için kullanılıyor.

- Snort: IP ağlarında gerçek zamanlı analiz ve paket günlüğü gerçekleştirebilen yazılımdır.
    1. Zararlıya özel kelimeler zararlı içerisinden strings komutuyla çıkarılır.
    2. En spesifik olanlar .yara dosyasının strings kısmına eklenmek için not edilir.
    3. .yara dosyası toplanan verilerle doldurulur.
    4. yara komutuyla zararlı test edilir.

- Automated Analysis Platforms
    1. www.virustotal.com
    2. app.any.run 
    3. hybrid-analysis.com

- Azorult Stealer Analysis 3.3
  

## Offensive Threat Intelligence

- Baldr Stealer
    1. Effective information stealer
    2. Shotgun file grabbing?
    3. CLR Loading?
    4. github.com/ege_balci
    5. Malware Suck

## Sertifikasyon Programları
    1. OSCP ve alt başlıkları
    2. TSE
    3. OSCE
    4. GREM
