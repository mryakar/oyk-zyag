### Android Zararlı Analizi

    Zygote
        - RAM üzerinde Dalvik VM çalıştırır.
        - Sıkça kullanılan kütüphaneleri yükler.
        - Yeni bir uygulamayla kendini fork eder.
        - Yeni uygulamaların açılması için socket açar ve dinler.

    - Her uygulama kendi userid'si vardır. Bu sayede erişebileceği veri ve dosya yapısına erişim sınırlandırılır.
    
    APK
        - Android Application Package
        - Farklı yollarla oluşturulabilir. Java, Kotlin, Xamarin, Unity
        - PlayStore'a konulan uygulamalar geliştirici tarafından imzalanmak zorundadır.
        - AndroidManifest: MainActivity, service, version, permissions...
        - Lib: Native kütüphane kodları
        - Res: resources.arsc'nin içinde olmayan res dosyaları
        - Resources.arsc: precompiled resources
        - META-INF: İmzada kullanılan key
        - Assets: Uygulama binary, image vs. farklı dosyalar varsa.
        - JNI Bridge

    - http://icyberchef.com/
