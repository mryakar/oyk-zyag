### Anahtar Noktalar
 
- Frida https://frida.re/

# Kilit Soru: Hiç yetki gerektirmeden bankacılık zararlı yazılımı nasıl yazılır?

    - /proc/pid nin altını çalışan her android yazılımı görebiliyor.
    - Birinci öncelik tuş vuruşlarını yakalayabilmek. İkinciside exfiltration.
    - Process boyutu başılan tuşa göre değişiyor. Bu şekilde tuşları kaydedebiliyoruz.
    - ping gibi her yerden kullanılabilen programlarla komuta kontrol sunucusuna internet yetkisi olmadan veri iletiliyor.
    
- Generic -> APT -> APT + 0d -> APT + Side

- Vault 7 Leaks WikiLeaks

- Sample Websites

    1. https://koodous.com/
    2. https://www.apklab.io/
    3. https://app.apkdetect.com/
