import "pe"

rule nanocore : rat {
    meta:
        description = "Nanocore Rat"
        in_the_wild = true
	threat_level = 8
    strings:
	$s1 = "runas"
	$s2 = "EnableLUA"
	$s3 = "SendToServer"
	$s4 = "No instance of plugin '{0}' could be found."
	$s5 = "Client assembly does not meet plugin type requirements."
	$s6 = "522D4B77-C8AA-9D54-CF406F661572"
    condition:
	(($s6 or $s5 or $s4) or (3 of them))
}
