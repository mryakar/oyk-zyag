### Zararlı Yazılım Tipleri

Virüs
Worm
Trojan
Keylogger
Ransomware
Rootkit
Bootkit
Dropper/Downloader
    Dolaylı yoldan zararlı yazılım indiren zararlı yazılımlardır.
Clipper 
    Bulaştığı sistemde clipboard'u hedef alan yazılımlardır.
Adware

VX Heaven= Zaralı yazılım konusunda kullanılan ilk forum sitesi.

Zaralı Yazılım Analisti İş Tanımı

    1. Analysis
    2. Detection
    3. Prevention

### OS ve Temel Bİlgiler

- Modern Operating Systems 3rd Edition  Andrew S. Tanenbaum

Çalıştırılabilir Dosya Formatı
    PE(Portable Executable) dosyası nasıl yazılır?
        
Assembly
    http://opensecuritytraining.info/IntroX86.html
    http://opensecuritytraining.info/IntroX86-64.html
    https://software.intel.com/sites/default/files/managed/39/c5/325462-sdm-vol-1-2abcd-3abcd.pdf

Endüstriyel Seviyede Kullanılan Debugger'lar

    IDA Pro
    Ghidra
    OllyDbg
    BinaryNinja

- Reversing: Secrets of Reverse Engineering
- Practical Malware Analysis
- Malware Analyst's Cookbook

Temel Windows Kalıcılık Sağlama Metotları
    
    1. Registry Keys
        UserCurrentVersionRun anahtarı ile bir program otomatik çalıştırılabilir.
    2. Services
    3. DLL Highjacking
    4. Shortcut(.LNK) Hijacking
    5. COM Hijacking
    6. Binary Patching
    7. Browser Plugins

### Hidden Tear Analysis

Ransomware analizi. Kaynak kodu paylaşılan ilk ransomware. Kaynak kodu hatalı olarak implement edilmiş.

    1. Yeni AES anatarı oluştur.
    2. Dosya sistemini şifrele.
    3. AES anahtarını asimetrik olarak şifrele ve dosya sistemine ekle.
    4. Rastgelelik için sistem saati referans alınmamalı.
























